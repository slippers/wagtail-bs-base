from django.apps import AppConfig


class WagtailBootstrapBaseConfig(AppConfig):
    name = 'wagtail_bs_base'
    verbose_name = 'Wagtail Boostrap Base'
    default_auto_field = 'django.db.models.AutoField'

    
