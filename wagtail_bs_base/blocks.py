from django.utils.translation import gettext_lazy as _
from wagtail.blocks import CharBlock, StructBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtailmarkdown.utils import render_markdown


CLASS_MAX_LENGTH = 255


"""
inject markdown blocks into wagtail-bs-blocks layouts


from wagtail_bs_base.blocks import MarkdownDocumentBlock
from wagtailmarkdown.blocks import MarkdownBlock

WAGTAIL_BS_BLOCKS_EXTRA = [
    ('markdown', MarkdownBlock()),
    ('markdown_document', MarkdownDocumentBlock()),
    ]

"""
class MarkdownDocumentBlock(StructBlock):
    class Meta:
        help_text = _("Markdown document")
        template = 'wagtail_bs_base/blocks/markdown_document_block.html'
        icon = 'doc-full-inverse'

    document_src = DocumentChooserBlock(
            required=True,)

    markdown_class = CharBlock(
            required=False,
            label=('Markdown css class'),
            max_length=CLASS_MAX_LENGTH,
        )

    def render(self, value, context=None):
        if not value['document_src']:
            return
        with value['document_src'].open_file() as handler:
            content = handler.read()
            return render_markdown(content)
