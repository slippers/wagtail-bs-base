from django.utils.translation import gettext_lazy as _
from django.utils.html import strip_tags
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.search import index
from wagtail.admin.panels import (
    MultiFieldPanel,
    FieldPanel 
)
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel
from wagtail_bs_blocks.blocks import get_layout_blocks

class BasicStreamPage(MenuPageMixin, Page):

    class Meta:
        verbose_name = _('Basic Stream Bootstrap Page')

    content = StreamField(
            get_layout_blocks(),
            blank=True,
            null=True,
            use_json_field=True)

    @property
    def get_searchable_content(self):
        return strip_tags(self.content.render_as_block()).replace('\n', ' ').strip()

    search_fields = Page.search_fields + [
        index.SearchField('get_searchable_content'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('content'),
        ], 'Content'),
    ]

    settings_panels = Page.settings_panels + [
        menupage_panel
    ]
