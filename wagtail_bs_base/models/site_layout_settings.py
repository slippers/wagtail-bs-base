from django.utils.translation import gettext_lazy as _
from wagtail.contrib.settings.models import BaseSiteSetting, register_setting
from wagtail.fields import StreamField
from wagtail.admin.panels import (
    FieldPanel,
    TabbedInterface,
    ObjectList
)
from wagtail_bs_blocks.blocks import get_layout_blocks


@register_setting(icon='placeholder')
class SiteLayoutSettings(BaseSiteSetting):

    class Meta:
        verbose_name = _('Site Bootstrap Layout')

    header = StreamField(
        get_layout_blocks(),
        blank=True,
        null=True,
        use_json_field=True,
        help_text=_('Header'))

    header_panels = [FieldPanel('header')]

    footer = StreamField(
        get_layout_blocks(),
        blank=True,
        null=True,
        use_json_field=True,
        help_text=_('Footer'))

    footer_panels = [FieldPanel('footer')]

    edit_handler = TabbedInterface([
        ObjectList(header_panels, heading=_('Header')),
        ObjectList(footer_panels, heading=_('Footer')),
    ])
