from pathlib import Path
import os
from django.utils.translation import gettext_lazy as _
from django import template as django_template
from django.utils.functional import cached_property
from django.contrib.contenttypes.models import ContentType
from django.db.models import CharField
from wagtail.admin.panels import (
        FieldPanel,
        MultiFieldPanel,
        HelpPanel,
        )
from .basic_stream_page import BasicStreamPage
import pdb

class TemplateStreamPage(BasicStreamPage):

    class Meta:
        verbose_name = _('Template Stream Bootstrap Page')

    custom_template = CharField(
            blank=True,
            max_length=255,
            choices=None,
            verbose_name=_('Template')
            )

    settings_panels = BasicStreamPage.settings_panels + [
            MultiFieldPanel(
                [
                    HelpPanel(_('locate templates in wagtail_bs_base that start with web_*.html')),
                    FieldPanel('custom_template')
                    ],
                heading=_('Visual Design')
                ),
            ]

    @staticmethod
    def get_all_app_templates_files(app_label, find='**/*.html'):
        """return a tuple of template (dir, file) as posix paths
        """
        dirs = []
        for engine in django_template.loader.engines.all():
            template_dirs = [Path(os.fspath(p)) for p in engine.template_dirs]
            dirs.extend(x for x in template_dirs if app_label in str(x))
        template_files = []
        for xray in dirs:
            template_files.extend((xray, x) for x in Path(xray).glob(find))
        return template_files 

    @cached_property
    def web_templates(self):
        choices = [('', 'Default')]
        content_type = ContentType.objects.get_for_model(self.__class__) 
        template_files = TemplateStreamPage.get_all_app_templates_files(content_type.app_label, '*/web_*.html')
        for template_dir, template_file in template_files:
            template = str(template_file).replace(str(template_dir) + '/', '')
            choices.append((template, template))
        return choices

    def get_template(self, request, *args, **kwargs):
        if self.custom_template:
            return self.custom_template
        return super().get_template(request, args, kwargs)

    def __init__(self, *args, **kwargs):
        """
        Inject custom choices and defaults into the form fields
        to enable customization by subclasses.
        """
        super().__init__(*args, **kwargs)
        self._meta.get_field('custom_template').choices = self.web_templates



